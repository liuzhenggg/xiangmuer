package com.zw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zw.dao.IUserDAO;
import com.zw.entity.User;

@Service
public class UserService {
	@Autowired
	private IUserDAO UserDAO;

	public User selectUserByUsernameAndPassword(User user) {
		User exsistUser = UserDAO.selectUserByUsernameAndPassword(user);
		return exsistUser;
	}

	public Integer addUser(User user) {
		Integer flag = UserDAO.addUser(user);
		return flag;
	}

	

}
