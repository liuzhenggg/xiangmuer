package com.zw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zw.dao.IPriceDAO;

@Service
public class PriceService {
	
	@Autowired
	private IPriceDAO priceDAO;
	public Integer selectPrice(Integer attr_id) {
		Integer price = priceDAO.selectPrice(attr_id);
		return price;
	}

}
