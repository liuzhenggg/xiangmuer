package com.zw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zw.dao.IAttrInfoDAO;
import com.zw.dao.IProductDAO;
import com.zw.entity.Product;

@Service
public class InfoService {
	@Autowired
	private IProductDAO iProductDAO;
	@Autowired
	private IAttrInfoDAO iAttrInfoDAO;
	public Product selectProductById(Integer productId) {
		Product productById = iProductDAO.selectProductById(productId);
		return productById;
	}
	public String selectAttrNameById(Integer attr_id) {
		String attrName = iAttrInfoDAO.selectAttrNameById(attr_id);
		return attrName;
	}

}
