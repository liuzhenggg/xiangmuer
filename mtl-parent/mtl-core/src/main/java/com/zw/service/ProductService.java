package com.zw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zw.dao.IProductDAO;
import com.zw.entity.PageBean;
import com.zw.entity.PageQuery;
import com.zw.entity.Product;

@Service
public class ProductService {
	@Autowired
	private IProductDAO productDAO;
	
	public PageBean<Product> selectProductListByPage(Integer currentPage){
		PageBean<Product> pageBean = new PageBean<Product>();
		//设置当前页数
		pageBean.setCurrentPage(currentPage);
		//设置每页记录数
		int pageSize=4;
		pageBean.setPageSize(pageSize);
		//设置总记录数
		int totalSize = selectTotalSize();
		pageBean.setTotalSize(totalSize);
		//设置总页数
		int totalPage = (totalSize % pageSize == 0)?totalSize / pageSize:totalSize / pageSize + 1;
		pageBean.setTotalPage(totalPage);
		//设置当前页数据
		int begin = (currentPage-1)*pageSize;
		List<Product> list = productDAO.selectProductListByPage(new PageQuery(begin,pageSize));
		pageBean.setList(list);
		return pageBean;
	}
	public Integer selectTotalSize() {
		return productDAO.selectTotalSize();
	}
	

}
