package com.zw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zw.dao.IAttrTypeDAO;
import com.zw.entity.AttrType;

@Service
public class TypeService {
	@Autowired
	private IAttrTypeDAO attrTypeDAO;
	public  List<AttrType> selectTypeListByFirstStep(){
	   List<AttrType> listByFirstStep = attrTypeDAO.selectTypeListByFirstStep();
	   return listByFirstStep;
	}
	public  List<AttrType> selectTypeListBySecondStep(){
		   List<AttrType> listByFirstStep = attrTypeDAO.selectTypeListBySecondStep();
		   return listByFirstStep;
		}
	public  List<AttrType> selectTypeListByThirdStep(){
		   List<AttrType> listByFirstStep = attrTypeDAO.selectTypeListByThirdStep();
		   return listByFirstStep;
		}

}
