package com.zw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zw.dao.ICartDAO;
import com.zw.entity.Cart;

@Service
public class CartService {
	@Autowired
	private ICartDAO iCartDAO;
	public void insertCart(Cart cart) {
		 iCartDAO.insertCart(cart);
		
	}
	public List<Cart> selectCartList(){
	    List<Cart> cartList = iCartDAO.selectCartList();
	    return cartList;
	}
	public Integer selectCount() {
		Integer count = iCartDAO.selectCount();
		return count;
	}

}
