package com.zw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zw.dao.IBrandDAO;
import com.zw.entity.Brand;

@Service
public class BrandService {
	@Autowired
	private IBrandDAO brandDAO;
	public List<Brand> selectAllBrand(){
		List<Brand> brand = brandDAO.selectAllBrand();
		return brand;
	}
}
