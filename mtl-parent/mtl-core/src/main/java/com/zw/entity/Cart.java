package com.zw.entity;

public class Cart {
	private Integer cart_id;
	private Integer cart_productId;
	private String cart_productName;
	private String cart_imageId;
	private String cart_subTypeIds;
	private Integer cart_price;

	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Cart(Integer cart_id, Integer cart_productId, String cart_productName, String cart_imageId,
			String cart_subTypeIds, Integer cart_price) {
		super();
		this.cart_id = cart_id;
		this.cart_productId = cart_productId;
		this.cart_productName = cart_productName;
		this.cart_imageId = cart_imageId;
		this.cart_subTypeIds = cart_subTypeIds;
		this.cart_price = cart_price;
	}
	@Override
	public String toString() {
		return "Cart [cart_id=" + cart_id + ", cart_productId=" + cart_productId + ", cart_productName="
				+ cart_productName + ", cart_imageId=" + cart_imageId + ", cart_subTypeIds=" + cart_subTypeIds
				+ ", cart_price=" + cart_price + "]";
	}
	public Integer getCart_id() {
		return cart_id;
	}
	public void setCart_id(Integer cart_id) {
		this.cart_id = cart_id;
	}
	public Integer getCart_productId() {
		return cart_productId;
	}
	public void setCart_productId(Integer cart_productId) {
		this.cart_productId = cart_productId;
	}
	public String getCart_productName() {
		return cart_productName;
	}
	public void setCart_productName(String cart_productName) {
		this.cart_productName = cart_productName;
	}
	public String getCart_imageId() {
		return cart_imageId;
	}
	public void setCart_imageId(String cart_imageId) {
		this.cart_imageId = cart_imageId;
	}
	public String getCart_subTypeIds() {
		return cart_subTypeIds;
	}
	public void setCart_subTypeIds(String cart_subTypeIds) {
		this.cart_subTypeIds = cart_subTypeIds;
	}
	public Integer getCart_price() {
		return cart_price;
	}
	public void setCart_price(Integer cart_price) {
		this.cart_price = cart_price;
	}
	

	


}
