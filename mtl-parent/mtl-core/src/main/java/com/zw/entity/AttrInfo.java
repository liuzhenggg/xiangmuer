package com.zw.entity;

import java.util.List;

public class AttrInfo {
	private Integer attr_id;
	private String attr_name;
	private String attr_des;
	private Integer type_id;
	private String bak1;
	public AttrInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AttrInfo(Integer attr_id, String attr_name, String attr_des, Integer type_id, String bak1) {
		super();
		this.attr_id = attr_id;
		this.attr_name = attr_name;
		this.attr_des = attr_des;
		this.type_id = type_id;
		this.bak1 = bak1;
	}
	public Integer getAttr_id() {
		return attr_id;
	}
	public void setAttr_id(Integer attr_id) {
		this.attr_id = attr_id;
	}
	public String getAttr_name() {
		return attr_name;
	}
	public void setAttr_name(String attr_name) {
		this.attr_name = attr_name;
	}
	public String getAttr_des() {
		return attr_des;
	}
	public void setAttr_des(String attr_des) {
		this.attr_des = attr_des;
	}
	public Integer getType_id() {
		return type_id;
	}
	public void setType_id(Integer type_id) {
		this.type_id = type_id;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	@Override
	public String toString() {
		return "AttrInfo [attr_id=" + attr_id + ", attr_name=" + attr_name + ", attr_des=" + attr_des + ", type_id="
				+ type_id + ", bak1=" + bak1 + "]";
	}
	


}
