package com.zw.entity;

public class Product {
	
	private Integer productId;//唯一标识
	private String productCode;//产品编号
	private String productName;//产品名称
	private Integer productType;//产品类型
	private Integer productBrand;//产品品牌
	private String imageId;//产品图片
	private Integer productMinPrice;//产品最低回收价
	private String bak1;//预留字段
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(Integer productId, String productCode, String productName, Integer productType, Integer productBrand,
			String imageId, Integer productMinPrice, String bak1) {
		super();
		this.productId = productId;
		this.productCode = productCode;
		this.productName = productName;
		this.productType = productType;
		this.productBrand = productBrand;
		this.imageId = imageId;
		this.productMinPrice = productMinPrice;
		this.bak1 = bak1;
	}
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productCode=" + productCode + ", productName=" + productName
				+ ", productType=" + productType + ", productBrand=" + productBrand + ", imageId=" + imageId
				+ ", productMinPrice=" + productMinPrice + ", bak1=" + bak1 + "]";
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getProductType() {
		return productType;
	}
	public void setProductType(Integer productType) {
		this.productType = productType;
	}
	public Integer getProductBrand() {
		return productBrand;
	}
	public void setProductBrand(Integer productBrand) {
		this.productBrand = productBrand;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public Integer getProductMinPrice() {
		return productMinPrice;
	}
	public void setProductMinPrice(Integer productMinPrice) {
		this.productMinPrice = productMinPrice;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	
}
