package com.zw.entity;

public class ShoppingCart {
	private int[] subIds;
	private int price;
	public int[] getSubIds() {
		return subIds;
	}
	public void setSubIds(int[] subIds) {
		this.subIds = subIds;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
}
