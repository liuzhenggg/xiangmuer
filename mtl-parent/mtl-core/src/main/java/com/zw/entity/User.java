package com.zw.entity;

public class User {
	
	private Integer user_id;//唯一标识
	private String user_name;//用户名
	private String user_password;//密码
	private String user_phone;//手机号码
	private String user_email;//邮箱
	private String img_id;//头像图片地址
	private String user_type;//用户类型  0代表会员 1代表管理员
	private String user_alias;//用户昵称
	private String password_salt;//密码盐
	private String bak1;//预留字段
	/**
	 * 
	 */
	public User() {
		super();
		
	}
	/**
	 * @param user_id
	 * @param user_name
	 * @param user_password
	 * @param user_phone
	 * @param user_email
	 * @param img_id
	 * @param user_type
	 * @param user_alias
	 * @param password_salt
	 * @param bak1
	 */
	public User(Integer user_id, String user_name, String user_password, String user_phone, String user_email,
			String img_id, String user_type, String user_alias, String password_salt, String bak1) {
		super();
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_password = user_password;
		this.user_phone = user_phone;
		this.user_email = user_email;
		this.img_id = img_id;
		this.user_type = user_type;
		this.user_alias = user_alias;
		this.password_salt = password_salt;
		this.bak1 = bak1;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getImg_id() {
		return img_id;
	}
	public void setImg_id(String img_id) {
		this.img_id = img_id;
	}
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	public String getUser_alias() {
		return user_alias;
	}
	public void setUser_alias(String user_alias) {
		this.user_alias = user_alias;
	}
	public String getPassword_salt() {
		return password_salt;
	}
	public void setPassword_salt(String password_salt) {
		this.password_salt = password_salt;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", user_name=" + user_name + ", user_password=" + user_password
				+ ", user_phone=" + user_phone + ", user_email=" + user_email + ", img_id=" + img_id + ", user_type="
				+ user_type + ", user_alias=" + user_alias + ", password_salt=" + password_salt + ", bak1=" + bak1
				+ "]";
	}
			
	}
