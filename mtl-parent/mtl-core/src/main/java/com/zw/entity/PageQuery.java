package com.zw.entity;

public class PageQuery {
	private Integer begin;//从第几个开始
	private Integer pageSize;//查找几个
	public PageQuery() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PageQuery(Integer begin, Integer pageSize) {
		super();
		this.begin = begin;
		this.pageSize = pageSize;
	}
	public Integer getBegin() {
		return begin;
	}
	public void setBegin(Integer begin) {
		this.begin = begin;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	@Override
	public String toString() {
		return "PageQuery [begin=" + begin + ", pageSize=" + pageSize + "]";
	}

}
