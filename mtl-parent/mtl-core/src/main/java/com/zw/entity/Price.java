package com.zw.entity;

public class Price {
	private Integer discount_id;
	private Integer product_id;
	private Integer attr_id;
	private Integer discount_price;
	private String discount_desc;
	private String bak1;

	public Price() {
		super();
		// TODO Auto-generated constructor stub
	}
public Price(Integer discount_id, Integer product_id, Integer attr_id, Integer discount_price, String discount_desc,
		String bak1) {
	super();
	this.discount_id = discount_id;
	this.product_id = product_id;
	this.attr_id = attr_id;
	this.discount_price = discount_price;
	this.discount_desc = discount_desc;
	this.bak1 = bak1;
}
public Integer getDiscount_id() {
	return discount_id;
}
public void setDiscount_id(Integer discount_id) {
	this.discount_id = discount_id;
}
public Integer getProduct_id() {
	return product_id;
}
public void setProduct_id(Integer product_id) {
	this.product_id = product_id;
}
public Integer getAttr_id() {
	return attr_id;
}
public void setAttr_id(Integer attr_id) {
	this.attr_id = attr_id;
}
public Integer getDiscount_price() {
	return discount_price;
}
public void setDiscount_price(Integer discount_price) {
	this.discount_price = discount_price;
}
public String getDiscount_desc() {
	return discount_desc;
}
public void setDiscount_desc(String discount_desc) {
	this.discount_desc = discount_desc;
}
public String getBak1() {
	return bak1;
}
public void setBak1(String bak1) {
	this.bak1 = bak1;
}
@Override
public String toString() {
	return "Price [discount_id=" + discount_id + ", product_id=" + product_id + ", attr_id=" + attr_id
			+ ", discount_price=" + discount_price + ", discount_desc=" + discount_desc + ", bak1=" + bak1 + "]";
}

	

}
