package com.zw.entity;

import java.util.List;

public class AttrType {
	private Integer attr_type_id;
	private String attr_type_name;
	private Integer attr_step;
	private String bak1;
	private List<AttrInfo> attrInfo;
	public AttrType() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AttrType(Integer attr_type_id, String attr_type_name, Integer attr_step, String bak1,
			List<AttrInfo> attrInfo) {
		super();
		this.attr_type_id = attr_type_id;
		this.attr_type_name = attr_type_name;
		this.attr_step = attr_step;
		this.bak1 = bak1;
		this.attrInfo = attrInfo;
	}
	public Integer getAttr_type_id() {
		return attr_type_id;
	}
	public void setAttr_type_id(Integer attr_type_id) {
		this.attr_type_id = attr_type_id;
	}
	public String getAttr_type_name() {
		return attr_type_name;
	}
	public void setAttr_type_name(String attr_type_name) {
		this.attr_type_name = attr_type_name;
	}
	public Integer getAttr_step() {
		return attr_step;
	}
	public void setAttr_step(Integer attr_step) {
		this.attr_step = attr_step;
	}
	public String getBak1() {
		return bak1;
	}
	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}
	public List<AttrInfo> getAttrInfo() {
		return attrInfo;
	}
	public void setAttrInfo(List<AttrInfo> attrInfo) {
		this.attrInfo = attrInfo;
	}
	@Override
	public String toString() {
		return "AttrType [attr_type_id=" + attr_type_id + ", attr_type_name=" + attr_type_name + ", attr_step="
				+ attr_step + ", bak1=" + bak1 + ", attrInfo=" + attrInfo + "]";
	}
	

}
