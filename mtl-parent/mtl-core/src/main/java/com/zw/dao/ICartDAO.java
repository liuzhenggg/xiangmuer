package com.zw.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.zw.entity.Cart;

@Component
public interface ICartDAO {
	//添加购物车
	public void insertCart(Cart cart);
	//查找购物车
	public List<Cart> selectCartList();
	//查找账户
	public Integer selectCount();
}
