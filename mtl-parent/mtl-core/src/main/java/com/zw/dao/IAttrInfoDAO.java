package com.zw.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.zw.entity.AttrInfo;

@Component
public interface IAttrInfoDAO {
	public List<AttrInfo> selectInfoListByTypeId(Integer attr_type_id);
	public String selectAttrNameById(Integer attr_id);

}
