package com.zw.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.zw.entity.AttrType;
@Component
public interface IAttrTypeDAO {
	public List<AttrType> selectTypeListByFirstStep();
	public List<AttrType> selectTypeListBySecondStep();
	public List<AttrType> selectTypeListByThirdStep();

}
