package com.zw.dao;

import org.springframework.stereotype.Component;

@Component
public interface IPriceDAO {
	
	public Integer selectPrice(Integer attr_id);

}
