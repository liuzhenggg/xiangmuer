package com.zw.dao;

import com.zw.entity.PageQuery;
import com.zw.entity.Product;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface IProductDAO {
	
	public List<Product> selectProductListByPage(PageQuery pageQuery);

	public Integer selectTotalSize();
	
	public Product selectProductById(Integer productId);

}
