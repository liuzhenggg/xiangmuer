package com.zw.dao;

import org.springframework.stereotype.Component;

import com.zw.entity.User;

@Component
public interface IUserDAO {
	public User selectUserByUsernameAndPassword(User user);
	
	public Integer addUser(User user);

}
