package com.zw.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.zw.entity.Brand;

@Component
public interface IBrandDAO {
	public List<Brand> selectAllBrand();

}
