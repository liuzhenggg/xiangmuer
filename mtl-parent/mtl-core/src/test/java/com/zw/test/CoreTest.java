package com.zw.test;

import com.zw.entity.*;
import com.zw.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:/spring-dao.xml","classpath*:/spring-service.xml"})
public class CoreTest {

	@Autowired
	private ProductService productService;
	@Autowired
	private BrandService brandService;
	@Autowired
	private InfoService infoService;
	@Autowired
	private UserService userService;
	@Autowired
	private TypeService typeService;
	@Autowired
	private PriceService priceService;
	@Autowired
	private CartService cartService;

	@Test
	public void selectProductListByPage() {
		PageBean<Product> pageBean = productService.selectProductListByPage(1);
		System.out.println(pageBean);
	}
	@Test
	public void selectTotalSize(){
		int size = productService.selectTotalSize();
		System.out.println(size);
	}
	@Test
	public void selectAllBrand() {
		List<Brand> brand = brandService.selectAllBrand();
		System.out.println(brand);
	}
	@Test
	public void selectProductById() {
//		Product productById = infoService.selectProductById(1);
		String AttrName = infoService.selectAttrNameById(1);
		System.out.println(AttrName);
//		System.out.println(productById);
	}
//	@Test
//	public void selectUser() {
//		User selectUser = userService.selectUserByUsernameAndPassword(new User(1,"zhangwei","cyfwlp"));
//		System.out.println(selectUser);
//	}
	
	@Test
	public void selectTypeListByFirstStep() {
		List<AttrType> listByFirstStep = typeService.selectTypeListByFirstStep();
		System.out.println(listByFirstStep);
	}
	@Test
	public void selectPrice() {
		Integer price = priceService.selectPrice(7);
		System.out.println(price);
	}
	@Test
	public void insertCart() {
//	cartService.insertCart(new Cart(1,1,"aaa","bbb","ccc",2));
	List<Cart> cartList = cartService.selectCartList();
	System.out.println(cartList);
	}
}
