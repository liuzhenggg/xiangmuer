package com.zw.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zw.entity.Brand;
import com.zw.entity.PageBean;
import com.zw.entity.Product;
import com.zw.service.BrandService;
import com.zw.service.ProductService;


@Controller
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	@Autowired
	private BrandService brandService;
	@RequestMapping("/selectProductListByPage")
	public String selectProductListByPage(Model model,Integer currentPage) {
		if(null == currentPage) {
			currentPage = 1;
		}
		PageBean<Product> pageBean = productService.selectProductListByPage(currentPage);
		List<Brand> allBrand = brandService.selectAllBrand();
		model.addAttribute("allBrand", allBrand);
		model.addAttribute("pageBean", pageBean);
		return "index";
	}

}
