package com.zw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zw.entity.Cart;
import com.zw.entity.Product;
import com.zw.service.CartService;
import com.zw.service.InfoService;

@Controller
public class CartController {
	@Autowired
	private CartService cartService;
	@Autowired
	private InfoService infoService;
	@RequestMapping("toCart.com")
	public String toCart(Model model,Integer phoneId,Integer price,String subTypeIds) {
		Product productById = infoService.selectProductById(phoneId);
		System.out.println(productById);
		model.addAttribute("phone", productById);
		model.addAttribute("price", price);
		String[] typeIds = subTypeIds.split(",");
		int[] typeId = new int[typeIds.length];
		String AttrName = "";
		for (int i = 0; i < typeIds.length; i++) {
			typeId[i]=Integer.parseInt(typeIds[i]);
			String AttrNameById = infoService.selectAttrNameById(typeId[i]);
			 AttrName = AttrName+"|"+AttrNameById ;
		}
//		attrName.add(AttrName);
//		System.out.println(AttrName);
//		String imageId = phone.getImageId();
		cartService.insertCart(new Cart(1,productById.getProductId(),productById.getProductName(),productById.getImageId(),AttrName,price));
		return "price";
	}

}
