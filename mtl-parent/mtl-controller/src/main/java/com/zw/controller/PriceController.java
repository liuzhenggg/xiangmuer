package com.zw.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zw.entity.Product;
import com.zw.entity.ShoppingCart;
import com.zw.service.InfoService;
import com.zw.service.PriceService;

@Controller
public class PriceController {
	@Autowired
	private InfoService infoService;
	@Autowired
	private PriceService priceService;
	@RequestMapping("/price")
	public String calcPrice(String subTypeIds,Integer goodId,Model model,HttpSession session) {
//		System.out.println(subTypeIds);
		Product productById = infoService.selectProductById(goodId);
		model.addAttribute("productById", productById);
//		System.out.println(productById);
		String[] arr = subTypeIds.split(",");
		int [] arr2 =new int[arr.length];
		//把字符串数组中的值，转换为int类型，放入int数组
		for(int n=0;n<arr.length;n++){
		arr2[n]=Integer.parseInt(arr[n]);
		}
		int priceCount = 0;
		for (int typeIds : arr2) {
//			System.out.println(typeIds);
			Integer price = priceService.selectPrice(typeIds);
			priceCount=priceCount+price;
		}
//		System.out.println(priceCount);
		model.addAttribute("priceCount", priceCount);
//		session.setAttribute("arrIds", subTypeIds);
		model.addAttribute("subTypeIds", subTypeIds);
		return "price";
	}
	
//	@RequestMapping("addCart")
//	public String addShoppingCart(HttpSession session) {
//		ShoppingCart cart = new ShoppingCart();
//		cart.setPrice(1);
//		cart.setSubIds(new int[1]);
//		List<ShoppingCart> carts = new ArrayList();
//		session.setAttribute("carts", carts);
//		return"..";
//	}
//	@RequestMapping("cart")
//	public String getShopping(HttpSession session) {
//		List<ShoppingCart>carts = (ArrayList)session.getAttribute("carts");
//		return "页面";
//	}

}
