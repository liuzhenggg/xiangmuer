package com.zw.controller;

import com.zw.entity.Brand;
import com.zw.entity.Cart;
import com.zw.entity.PageBean;
import com.zw.entity.Product;
import com.zw.service.BrandService;
import com.zw.service.CartService;
import com.zw.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class IndexController {
	@Autowired
	private ProductService productService;
	@Autowired
	private BrandService brandService;
//	@Autowired
//	private InfoService infoService;
	@Autowired
	private CartService cartService;
	@RequestMapping("/toIndex.com")
	public String toIndex(Model model,Integer currentPage) throws Exception {
//		request.setCharacterEncoding("UTF-8");//传值编码
//		response.setContentType("text/html;charset=UTF-8");//设置传输编码
//		String phone1=new String(phone.getBytes("ISO-8859-1"),"utf-8");//get处理乱码
		if(null == currentPage) {
			currentPage = 1;
		}
		PageBean<Product> pageBean = productService.selectProductListByPage(currentPage);
		List<Brand> allBrand = brandService.selectAllBrand();
		model.addAttribute("allBrand", allBrand);
		model.addAttribute("pageBean", pageBean);
//		Product phone = infoService.selectProductById(phoneId);
//		System.out.println(phone);
//		cartService.insertCart(new Cart(1,phone.getProductId(),phone.getProductName(),phone.getImageId(),subTypeIds,price));
		List<Cart> cartList = cartService.selectCartList();
//		System.out.println(cartList);
		Integer countPrice = 0;
		for (Cart cart : cartList) {
			Integer cart_price = cart.getCart_price();
			countPrice = countPrice+cart_price;
		}
		Integer count = cartService.selectCount();
		model.addAttribute("count", count);
		model.addAttribute("cartList", cartList);
		model.addAttribute("countPrice", countPrice);
		System.out.println("-----");
		return "index";
	}

}
