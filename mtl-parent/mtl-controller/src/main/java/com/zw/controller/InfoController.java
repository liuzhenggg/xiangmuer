package com.zw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zw.entity.AttrType;
import com.zw.entity.Product;
import com.zw.service.InfoService;
import com.zw.service.TypeService;

@Controller
@RequestMapping("/info")
public class InfoController {
	@Autowired
	private InfoService infoService;
	@Autowired
	private TypeService typeService;
	@RequestMapping("/attrType")
	public String attrType(Model model,Integer productId) {

		Product productById = infoService.selectProductById(productId);
		List<AttrType> listByFirstStep = typeService.selectTypeListByFirstStep();
		List<AttrType> listBySecondStep = typeService.selectTypeListBySecondStep();
		List<AttrType> listByThirdStep = typeService.selectTypeListByThirdStep();
		model.addAttribute("listByFirstStep",listByFirstStep);
		model.addAttribute("listBySecondStep", listBySecondStep);
		model.addAttribute("listByThirdStep", listByThirdStep);

		model.addAttribute("productById", productById);
		return "info";
		
	}
	

}
