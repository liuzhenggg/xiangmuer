package com.zw.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zw.entity.Cart;
import com.zw.service.CartService;
import com.zw.service.InfoService;

@Controller
public class OrderController {
	@Autowired
	private CartService cartService;
	@Autowired
	private InfoService infoService;
	@RequestMapping("toOrder.com")
	public String toOrder(Model model,Integer countProduct,Integer countPrice) {
		model.addAttribute("countProduct", countProduct);
		model.addAttribute("countPrice", countPrice);
		List<Cart> cartList = cartService.selectCartList();
		model.addAttribute("cartList", cartList);
//		System.out.println(cartList);
//		List<String> attrName = new ArrayList<>();
		/*for (Cart cart : cartList) {
			String cart_subTypeIds = cart.getCart_subTypeIds();
			System.out.println(cart_subTypeIds);
			String[] typeIds = cart_subTypeIds.split(",");
			int[] typeId = new int[typeIds.length];
			String AttrName = "";
			for (int i = 0; i < typeIds.length; i++) {
				typeId[i]=Integer.parseInt(typeIds[i]);
				String AttrNameById = infoService.selectAttrNameById(typeId[i]);
				 AttrName = AttrName+"|"+AttrNameById ;
			}
//			attrName.add(AttrName);
			System.out.println(AttrName);
			model.addAttribute("AttrName", AttrName);
		}*/
//		System.out.println(AttrName);
//		model.addAttribute("AttrName", AttrName);
//		System.out.println(attrName);
//		model.addAttribute("attrName", attrName);
		return "cart";
	}
	@RequestMapping("toOrder1.com")
	public String toOrder1(Model model) {
//		model.addAttribute("countProduct", countProduct);
//		model.addAttribute("countPrice", countPrice);
		List<Cart> cartList = cartService.selectCartList();
		model.addAttribute("cartList", cartList);
		return "order";
	}

}
